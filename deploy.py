import requests


def deploy():
    response = requests.post('https://example.com/deploy')
    if response.status_code == 200:
        print('Deployment successful')
    else:
        print('Deployment failed')


if __name__ == '__main__':

    deploy()
